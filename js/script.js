var Mta = Mta || {};

Mta.Stops = [
              {
                "slocation" : "Astoria Ditmars Blvd",
                "transfer" : [ "W" ],
                "access" : " "
              },
              {
                "slocation" : "Astoria Blvd",
                "transfer" : [ "W" ],
                "access" : " "
              },
              {
                "slocation" : "30th Av",
                "transfer" : [ "W" ],
                "access" : " "
              },
              {
                "slocation" : "Broadway",
                "transfer" : [ "W" ],
                "access" : " "
              },
              {
                "slocation" : "36 Av",
                "transfer" : [ "W" ],
                "access" : " "
              },
              {
                "slocation" : "39 Av",
                "transfer" : [ "W" ],
                "access" : " "
              },
              {
                "slocation" : "21 St - Queensbridge",
                "transfer" : [ "W" ],
                "access" : "&"
              },
              {
                "slocation" : "Lexington Av/59 St",
                "transfer" : [ "6", "F", "R", "W" ],
                "access" : " "
              },
              {
                "slocation" : "5th Av/59 St",
                "transfer" : [ "R", "W" ],
                "access" : " "
              },
              {
                "slocation" : "57 St/7Av",
                "transfer" : [ "Q", "R", "W" ],
                "access" : " "
              },
              {
                "slocation" : "49 St",
                "transfer" : [ "1", "2", "3", "7", "A", "C", "E", "Q", "R", "W", "S" ],
                "access" : "&"
              },
              {
                "slocation" : "34 St - Herald Sq",
                "transfer" : [ "B", "D", "F", "R", "V", "W", "PATH" ],
                "access" : "&"
              },
              {
                "slocation" : "14 St - Union Sq",
                "transfer" : [ "4", "5", "6", "L", "Q", "R", "W" ],
                "access" : "&"
              },
              {
                 "slocation" : "Canal St",
                 "transfer" : [ "4", "6", "J", "M", "Q", "R", "W", "Z" ],
                 "access" : " "
               },
              {
                "slocation" : "Rector St",
                "transfer" : [ "W" ],
                "access" : " "
              },
              {
                 "slocation" : "Whitehall Street",
                 "transfer" : [ "1" ],
                 "access" : " "
               }
          ];

(function($){
  var self = this;

  self.Train = (function(){
    var pub = {},
        formattedStops = [],
        state = 0,

        VISIBLE_STOPS = 10;     //10 visible items/stops at time

    pub.start = function(){
        //format if not already formatted
        if($('#next').children().length === 0){
            for(var i = 0; i < Mta.Stops.length; i++){
                formattedStops[i] = _formatStop(Mta.Stops[i]);
            }
        }

        _appendStops('forward');
        _binds();
    };

    function _binds(){
      $("#next-b").on("click", function(e){
          if((Mta.Stops.length - state) > VISIBLE_STOPS ){
              state++;
              _appendStops('forward');
          }
      });
      $("#prev-b").on("click", function(e){
          _appendStops('reverse');
      });
    }

    //creates a temp array and appends it to the dom
    function _appendStops(direction){
        var tmpArray = [];

        if(direction != 'forward'){
            formattedStops.reverse();
            state = 0;
        }

        tmpArray = formattedStops.slice(state,state+VISIBLE_STOPS);

        //make sure we are working w/ a clean slate
        $("#train-map .stops-wrap #next").empty();

        for(var i=0; i<VISIBLE_STOPS; i++ ){
            $("#train-map #next").append(tmpArray[i]);
        }
    }

    //formats array objects
    function _formatStop(ob){
        var html = [];
        html.push('<div class="stop">');
        html.push('<span class="location">'+ob.slocation+'<\/span>');
        html.push('<span class="access">'+ob.access+'<\/span>');
        html.push('<span class="transfer">'+ob.transfer+'<\/span>');
        html.push('<hr />');
        html.push('<\/div>');

        return html.join(' ');
    }

    return pub;
  })();

  Mta.Train.start();

}).call(Mta, jQuery);
